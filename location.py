class Location(object):
    def __init__(self, _id, lat, lng):
        self.id = _id
        self.lat = lat
        self.lng = lng

    def getId(self):
        return self.id

    def getLat(self):
        return self.lat

    def getLng(self):
        return self.lng

    def getCoordinates(self):
        return self.lat, self.lng