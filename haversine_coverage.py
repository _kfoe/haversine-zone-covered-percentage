import data

from haversine import haversine
from location import Location
from coverage import Coverage
from shopper import Shopper
from collections import OrderedDict

class HeversineCoverage(object):
    def __init__(self):
        self.results = []
        self.coverage_founded = {}

    def run(self):
        for shopper_data in data.shoppers:
            '''
            Build shopper
            '''
            shopper = Shopper(shopper_data['id'], shopper_data['lat'], shopper_data['lng'], shopper_data['enabled'])

            '''
            Verify if shopper is enable
            '''
            if(shopper.getIsEnabled() is False):
                continue

            for location in data.locations:
                '''
                Build location
                '''
                location = Location(location['id'], location['lat'], location['lng'])

                '''
                Push only if distance < 10
                '''
                if haversine(shopper.getCoordinates(), location.getCoordinates()) < 10:
                    shopper.incrementLocation(1)
                    shopperId = shopper.getId()
                    '''
                    Push calculated coverage
                    '''
                    self.coverage_founded[shopperId] = Coverage(shopper).calculate()

        '''
        Order by distance
        '''
        for shopper, coverage in sorted(self.coverage_founded.items(), key=lambda k: k[1], reverse=True):
            self.results.append({'shopper_id': shopper, 'coverage': coverage})

        '''
        Return results
        '''
        return self.results