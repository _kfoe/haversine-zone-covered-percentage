import data

class Coverage(object):
    def __init__(self, shopper):
        self.shopper = shopper
        self.totalDataLocations = len(data.locations)

    def calculate(self):
        return ((self.shopper.getLocations() / self.totalDataLocations) * 100)

    def getShopper(self):
        return self.shopper