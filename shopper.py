class Shopper(object):
    def __init__(self, _id, lat, lng, enabled):
        self.id = _id
        self.lat = lat
        self.lng = lng
        self.enabled = enabled
        self.locations = 0

    def getId(self):
        return self.id

    def getCoordinates(self):
        return self.lat, self.lng

    def getIsEnabled(self):
        return self.enabled

    def getLocations(self):
        return self.locations

    def incrementLocation(self, count):
        self.locations += count